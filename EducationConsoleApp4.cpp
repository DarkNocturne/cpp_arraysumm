#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int daytoday = buf.tm_mday;

    const int ArraySizeX = 5;
    const int ArraySizeY = 5;
    int MainArray[ArraySizeX][ArraySizeY];
    
    for (int i = 0; i < ArraySizeX; i++) {
        for (int j = 0; j < ArraySizeY; j++) {
            MainArray[i][j] = i + j;
            std::cout << MainArray[i][j];
        }
        std::cout << "\n";
    }

    int ArrayRowSumm = 0;
    int ArrayRowSelected = daytoday % 5;
    for (int n = 0; n < ArraySizeX; n++) {
        ArrayRowSumm += MainArray[n][ArrayRowSelected];
    }
    std::cout << "\nDAY TODAY: " << daytoday <<  "\nROW: " << ArrayRowSelected << "\nSUMM: " << ArrayRowSumm << "\n";
}